module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		copy: {
			html: {
				files: [
      				{expand: true, src: ['**/*.html'], cwd: 'www/public/src/', dest: 'www/public/dist/', filter: 'isFile'},
      			],
  			},
			assets: {
				files: [
      				{expand: true, src: ['vendor/bootstrap/fonts/*'], flatten: true, dest: 'www/public/dist/fonts/', filter: 'isFile'},
      				{expand: true, src: ['www/public/src/images/*'], flatten: true, dest: 'www/public/dist/images', filter: 'isFile'},
      			],
  			},
		},

		less: {
			development:{
				options:{
					paths: ["vendor/bootstrap/less/","vendor/bootstrap-daterangepicker/", "vendor/"],
					plugins: [
				        new (require('less-plugin-autoprefix'))({browsers: ["last 2 versions"]})
				    ],
					compress: true
				},
				files:{
					"www/public/dist/css/style.css": "www/public/src/less/style.less"
				}
			}
		},
		concat: {
			options:{
				separator: ';',
			},
			dist:{
				src: ['vendor/jquery/dist/jquery.js', 'vendor/bootstrap/dist/js/bootstrap.js', 
						'vendor/angular/angular.js', 'vendor/angular-route/angular-route.js', 
						'vendor/moment/moment.js', 'vendor/bootstrap-daterangepicker/daterangepicker.js', 'vendor/angular-daterangepicker/js/angular-daterangepicker.js', 
						'www/public/src/js/script.js',
						'www/public/src/js/app.js', 'www/public/src/js/directives.js', 'www/public/src/js/controllers.js'],
				dest: 'www/public/dist/js/script.js'
			}
		},
		watchers: {
			options:{
				livereload: true
			},
			html:{
				files:['www/public/src/**/*.html'],
				tasks:['copy:html']
			},
			js:{
				files:['www/public/src/**/*.js'],
				tasks:['concat']
			},
			less:{
				files:['www/public/src/**/*.less'],
				tasks:['less']
			},
			gruntfile:{
				files: ['Gruntfile.js'],
				tasks: ['build']
			}
		},
		uglify: {
			options: {
				mangle: false,
				screwIE8: true,
    			banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
			},
			dist: {
				files: {
					'<%= concat.dist.dest %>': ['<%= concat.dist.dest %>']
				}
			}
		}
	});

	require('load-grunt-tasks')(grunt);

	grunt.renameTask( 'watch', 'watchers' );
	grunt.registerTask( 'watch', [ 'build', 'uglify', 'watchers' ] );
	grunt.registerTask('build', ['copy', 'less', 'concat']);
	grunt.registerTask('default', ['build', 'uglify']);
}

