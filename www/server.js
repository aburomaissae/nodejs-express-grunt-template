var express = require('express'),
session = require('express-session'),
bodyParser = require('body-parser'),
cookieParser = require('cookie-parser'),
passport = require('passport'),
LocalStrategy = require('passport-local').Strategy,
config = require('./config/settings.json'),
fs = require('fs-extra'),
app = express();

  //__dirname

passport.use(new LocalStrategy(
  function(username, password, done) {
    var MD5 = require('md5');
    if( config.users[username] && config.users[username] == MD5.digest_s(password) )
    //if (username === "admin" && password === "admin") // stupid example
      return done(null, {name: "admin"});

    return done(null, false, { message: 'Incorrect username.' });
  }
));

// Serialized and deserialized methods when got from session
passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

// Define a middleware function to be used for every secured routes
var auth = function(req, res, next){
  if (!req.isAuthenticated()) 
  	res.sendStatus(401);
  else
  	next();
};

app.use(bodyParser.json())
.use(bodyParser.urlencoded({extended:true}))
.use(cookieParser('Cookie-Secret'))
.use(session({
	secret: 'Session-Secret',
	resave: false,
	saveUninitialized: true
}))
.use(passport.initialize()) // Add passport initialization
.use(passport.session())
.use(express.static(__dirname + '/public/dist'))


// GET REQUESTS
.get('/islogged', auth, function(req, res){
	res.send(true);
})
// POST REQUESTS
.post('/login', passport.authenticate('local'), function(req, res) {
	res.send("req.user");
})
.post('/logout', function(req, res){
  req.logOut();
  res.sendStatus(200);
})

// REQUESTING DATA HANDLERS
;

var serverPort = process.env.PORT || config.server.port;

var server = app.listen(serverPort, function () {

  var host = server.address().address
  var port = server.address().port

  console.log('Listening at %s', serverPort)

});
