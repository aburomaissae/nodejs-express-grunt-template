var myApp = angular.module('myApp', ['ngRoute', 'daterangepicker'])
.config(['$routeProvider', '$httpProvider',
	function($routeProvider, $httpProvider) {

		var isLoggedIn = function($q, $timeout, $http, $location, $rootScope){
			// Initialize a new promise
			var deferred = $q.defer();
			$http.get('/islogged').success(function(){
				$rootScope.isLogged = true;
				deferred.resolve();
			}).error(function(){
				deferred.reject();
				$location.url('/login');
			});
			return deferred.promise;
		};

		$httpProvider.interceptors.push(function($q, $location) {
	      return {
	        response: function(response) {
	          return response;
	        },
	        responseError: function(response) {
	          if (response.status === 401)
	            $location.url('/login');
	          return $q.reject(response);
	        }
	      };
	    });

		$routeProvider.when('/home', {
			templateUrl: 'views/home.html',
			controller: 'HomeCtrl',
			resolve: {
	          loggedin: isLoggedIn
	        }
		}).when('/login', {
			templateUrl: 'views/login.html',
			controller: 'LoginCtrl'
		}).otherwise({
			redirectTo: '/home'
		});
	}
])
.run(['$rootScope', '$http', '$location', function($rootScope, $http, $location){
	$rootScope.isLogged = false;
	$rootScope.appName = "My NodeJS Application";

    // Logout function is available in any pages
    $rootScope.logout = function(){
    	$rootScope.message = 'Logged out.';
    	$http.post('/logout').success(function(){
    		window.location.reload( true );
    	});
    };
}]);

