myApp.controller('MainCtrl', ['$scope', '$http', function($scope, $http){
	console.log( 'MainCtrl' );
}])

.controller('LoginCtrl', ['$scope', '$http', '$location', '$rootScope', function($scope, $http, $location, $rootScope){
	if( $scope.isLogged ) $location.url('/home');
	$scope.login = function(){
		$http.post('/login', {
			username: $scope.user.username,
			password: $scope.user.password,
		})
		.success(function(user){
			// No error: authentication OK
			//$rootScope.message = 'Authentication successful!';
			$location.url('/home');
			$rootScope.isLogged = true;
			$scope.tryagain = false;
		})
		.error(function(){
			// Error: authentication failed
			// $rootScope.message = 'Authentication failed.';
			//$location.url('/login');
			$scope.tryagain = true;
		});
	};
}])

.controller('HomeCtrl', ['$scope', '$http', function($scope, $http){
	console.log( 'HomeCtrl' );
}]);